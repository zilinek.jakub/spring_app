##Základní info:
Aplikace pracují s databází aut.
Obsahuje 5 entit `MAKE`, `MODEL`, `CAR`, `TYPE`, `CATEGORY`.
Obsahuje jednoduché funkce `CREATE`, `UPDATE`, `DELETE`.
Vyhledávací funkce `FINDBYID`, `FINDALL`.
Entita `MODEL` obsahuje funkci `FindByTypeName` která najde 
všechny modely určeného typu.
###Diagram databáze
![Diagram](diagram.png)




### GitLab
Server : https://gitlab.fit.cvut.cz/zilinjak/tjv_sem_server.git<br>
Client : https://gitlab.fit.cvut.cz/zilinjak/tjv_sem_client.git

#####Kompilace programu:
Projekty jsou sestavovány pomocí Gradle, tudíž pro kompilaci stačí použití scriptu ./gradlew build.
Vygenerovaný spustitelný soubor je ke spuštění v ($project_repository)/build/libs

#####Kompilace:
Potřeba mít nastavenou JAVA_HOME v systémových proměnných, java verze 13+.
./gradlew build 
#####Spuštění 
cd build/libs;<br>
Server: java -jar TJV_Sem_Server-1.0-SNAPSHOT.jar<br>
Client: java -jar TJV_Sem_Client-1.0-SNAPSHOT.jar


### Ovladání klienta
  Uživatel dostane na výběr z entit takto :
```
	 1. CAR
	 2. CATEGORY
	 3. MAKE
	 4. MODEL
	 5. TYPE
        666. KONEC
```
Pro výběr entity nad kterou chceme pracovat musí uživatel napsat číselnou hodnotu proměnné.
 
 Následně po vybrání entity se uživatel dostane do menu, které se ho ptá co s Entitou budeme dělat.
 U Modelu je navíc možnost 0 - Které nám poskytne vyhledávání dle jmén typů 
```  
         1. Find by ID/VIN - hledání dle ID
 	 2. Find all       - vrací všechny entity
 	 3. Create         - vytvoří za použití uživatele Entitu kterou poté pošle do Databáze
 	 4. Update         - vytvoří za použití uživatele Entitu, která přemaže, updatuje entitu v DB na určitém ID
  	 5. Delete         - vymaže entitu z DB dle uživatelovy volby
  ```
Pro výber musí uživatel napsat celoučíselnou hodnotu volby, kterou chce použít.

### API Serveru
Server pro ka6dou entitu obsahuje sadu příkazů. Server běží na portu```8080```. 
Dotazy na server jsou následující
###### http://localhost:8085/api/{$entityName}/{$id} - Vyhledá podle proměnné id
###### http://localhost:8085/api/{$entityName}/all -   všechny data v databázi k dané entitě
###### http://localhost:8085/api/{$entityName}/delete/{$id} - Smaže entitu na dané proměnné id
###### http://localhost:8085/api/{$entityName}/update/{$id} - aktualizuje entitu na dané proměnné id
###### http://localhost:8085/api/{$entityName}/create - vytvoří entitu 
###### http://localhost:8085/api/model/byType/{typeName} - vrátí všechny model které obsahují typ s jménem proměnné {$typeName}

{$id} = id objektu s kterým pracujeme
{$entityName} = Jméno objektu s kterým pracujeme = model, type, car, category, make
<br>
<br>

U metod příkazů delete ,update ,create je potřeba poslat v hlavičce 
JSON který obsahuje data na vytvoření.
Když server narazí na vadná data, vrací ResponseStatusHttp.NOT_FOUND.
