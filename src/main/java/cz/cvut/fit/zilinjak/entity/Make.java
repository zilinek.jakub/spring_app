package cz.cvut.fit.zilinjak.entity;

import com.sun.istack.NotNull;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;

import javax.persistence.*;

@Entity
@Table(name = "VYROBCE")
public class Make {

    @Id
    @SequenceGenerator(name="seq2", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq2")
    @Column(name = "VYROBCE_ID")
    private int id;

    @Column (name = "VYROBCE_NAZEV")
    private String name;

    @Column (name = "VYROBCE_SIDLO")
    private String place;

    public Make() {
    }
    public Make(String name, String place) {
        this.name = name;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Make that = (Make) obj;
        return id == that.id;
    }
}
