package cz.cvut.fit.zilinjak.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;



@Entity
@Table(name = "AUTOMOBIL")
public class Car {

    @Id
    @Column(name = "VIN")
    private String vin;
    @NotNull            // todo check NotNull
    @Column(name = "BARVA")
    private String color;
    @NotNull
    @Column(name = "CENA")
    private int price;
    @NotNull
    @Column(name = "STK")
    private boolean stk;

    @NotNull
    @Column(name = "DRUH_PALIVA")
    private String fuel_type; //TODO fuelType

    @NotNull
    @Column(name = "NAJETYCH_KM")
    private int driven;

    @ManyToOne
    @JoinColumn(name = "MODEL_ID")
    private Model model;

    @ManyToOne
    @JoinColumn(name = "KAT_ID")
    private Category category;
    public Car() {
    }

    public Car(String vin, String color, int price, boolean stk, String fuel_type, Model model, int driven, Category category) {
        this.vin = vin;
        this.color = color;
        this.price = price;
        this.stk = stk;
        this.fuel_type = fuel_type;
        this.model = model;
        this.driven = driven;
        this.category = category;
    }

    public int getDriven() {
        return driven;
    }
    public void setDriven(int driven) {
        this.driven = driven;
    }
    public String getVin() {
        return vin;
    }
    public void setVin(String vin) {
        this.vin = vin;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public boolean isStk() {
        return stk;
    }
    public void setStk(boolean stk) {
        this.stk = stk;
    }
    public String getFuel_type() {
        return fuel_type;
    }
    public void setFuel_type(String fuel_type) {
        this.fuel_type = fuel_type;
    }
    public Model getModel() {
        return model;
    }
    public void setModel(Model model) {
        this.model = model;
    }
    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Car that = (Car) obj;
        return vin.equals(that.vin);
    }
}
