package cz.cvut.fit.zilinjak.entity;

import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "DRUH_AUTOMOBILU")
public class Type {

    @Id
    @SequenceGenerator(name="seq4", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq4")
    @Column(name = "DRUH_ID")
    private int id;

    @NotNull
    @Column (name = "DRUH_NAZEV")
    private String name;


    public Type() {
    }

    public Type(String name) {
        this.name = name;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Type that = (Type) obj;
        return id == that.id;
    }
}
