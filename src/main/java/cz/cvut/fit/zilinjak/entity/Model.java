package cz.cvut.fit.zilinjak.entity;

import com.sun.istack.NotNull;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "MODEL")
public class Model {
    @Id
    @SequenceGenerator(name="seq3", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq3")
    @Column(name = "MODEL_ID")
    private int id;

    @NotNull
    @Column(name = "MODEL_NAZEV")
    private String name;

    @NotNull
    @Column(name = "ROK_VYROBY")
    Timestamp date;

    @ManyToOne
    @JoinColumn(name = "DRUH_ID")
    private Type type;

    @ManyToMany
    @JoinTable(
            name = "model_make",
            joinColumns = @JoinColumn(name = "model_id"),
            inverseJoinColumns = @JoinColumn(name = "make_id")
    )
    private List<Make> make;

    public Model(String name, Timestamp date, Type type, List<Make> make) {
        this.name = name;
        this.date = date;
        this.type = type;
        this.make = make;
    }

    public Model() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<Make> getMake() {
        return make;
    }

    public void setMake(List<Make> make) {
        this.make = make;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Model that = (Model) obj;
        return id == that.id;
    }
}
