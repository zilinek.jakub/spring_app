package cz.cvut.fit.zilinjak.entity;


import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity
@Table(name = "KATEGORIE")
public class Category {
    @Id
    @SequenceGenerator(name="seq1", initialValue=1, allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq1")
    @Column(name = "KAT_ID")
    private int id;

    @NotNull
    @Column(name = "KAT_NAZEV")
    private String name;


    public Category(String name) {
        this.name = name;
    }

    public Category() {
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        Category that = (Category) obj;
        return id == that.id;
    }
}
