package cz.cvut.fit.zilinjak.DTO;

import com.sun.istack.NotNull;
import cz.cvut.fit.zilinjak.entity.Make;

import javax.persistence.*;

public class MakeDTO {
    private final int id;
    private final String name;
    private final String place;

    public MakeDTO(int id, String name, String place) {
        this.id = id;
        this.name = name;
        this.place = place;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        MakeDTO that = (MakeDTO) obj;
        return id == that.id;
    }

    @Override
    public String toString() {
        return "id = " + id +
                ", name = '" + name + '\'' +
                ", place = '" + place + '\'';
    }
}
