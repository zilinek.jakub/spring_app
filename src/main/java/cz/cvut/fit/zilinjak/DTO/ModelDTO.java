package cz.cvut.fit.zilinjak.DTO;

import com.sun.istack.NotNull;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Type;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

public class ModelDTO {
    private   final  int id;
    private   final  String name;
    Timestamp        date;
    private   final  int type_id;
    private   final List<Integer> make_id;

    public ModelDTO(int id, String name, Timestamp date, int type_id, List<Integer> make_id) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.type_id = type_id;
        this.make_id = make_id;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public Timestamp getDate() {
        return date;
    }
    public int getType_id() {
        return type_id;
    }
    public List<Integer> getMake_id() {
        return make_id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        ModelDTO that = (ModelDTO) obj;
        return id == that.id;
    }

    @Override
    public String toString() {
        return  "id = " + id +
                ", name = " + name +
                ", date = " + date +
                ", type_id = " + type_id +
                ", make_id = " + make_id;
    }
}
