package cz.cvut.fit.zilinjak.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeCreateDTO {
    @JsonProperty("place")
    private final String place;
    @JsonProperty("name")
    private final String name;

    @JsonCreator
    public MakeCreateDTO(String name, String place) {
        this.name = name;
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public String getPlace() {
        return place;
    }
}
