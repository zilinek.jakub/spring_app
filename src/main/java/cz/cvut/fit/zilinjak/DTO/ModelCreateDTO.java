package cz.cvut.fit.zilinjak.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import cz.cvut.fit.zilinjak.entity.Model;

import java.sql.Timestamp;
import java.util.List;

public class ModelCreateDTO {
    @JsonProperty("name")
    private    String name;
    @JsonProperty("date")
    Timestamp        date;
    @JsonProperty("type")
    private    int type_id;
    @JsonProperty("make")
    private    List<Integer> make_id;

    @JsonCreator
    public ModelCreateDTO(String name, Timestamp date, int type_id, List<Integer> make_id) {
        this.name = name;
        this.date = date;
        this.type_id = type_id;
        this.make_id = make_id;
    }

    public ModelCreateDTO() {
    }

    public String getName() {
        return name;
    }
    public Timestamp getDate() {
        return date;
    }
    public int getType_id() {
        return type_id;
    }
    public List<Integer> getMake_id() {
        return make_id;
    }
}
