package cz.cvut.fit.zilinjak.DTO;

import cz.cvut.fit.zilinjak.entity.Car;

public class CarDTO {
    private final String vin;
    private final String color;
    private final int price;
    private final boolean stk;
    private final int category_id;
    private final String fuel_type;
    private final int model_id;
    private final int driven;


    public CarDTO(String vin, String color, int price, boolean stk, String fuel_type, int model_id, int category_id, int driven) {
        this.vin = vin;
        this.color = color;
        this.price = price;
        this.stk = stk;
        this.fuel_type = fuel_type;
        this.model_id = model_id;
        this.category_id = category_id;
        this.driven = driven;
    }

    public int getDriven() {
        return driven;
    }

    public String getVin() {
        return vin;
    }

    public String getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    public boolean isStk() {
        return stk;
    }

    public int getCategory_id() {
        return category_id;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public int getModel_id() {
        return model_id;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        CarDTO that = (CarDTO) obj;
        return vin.equals(that.vin);
    }

    @Override
    public String toString() {
        return  "vin = '" + vin + '\'' +
                ", color = '" + color + '\'' +
                ", price = " + price +
                ", stk = " + stk +
                ", category_id = " + category_id +
                ", fuel_type = '" + fuel_type + '\'' +
                ", model_id = " + model_id +
                ", driven = " + driven;
    }
}
