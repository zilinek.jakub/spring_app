package cz.cvut.fit.zilinjak.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.sun.istack.NotNull;
import cz.cvut.fit.zilinjak.entity.Type;

import javax.persistence.*;

public class TypeDTO {

    private final int id;
    private final String name;


    public TypeDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        TypeDTO that = (TypeDTO) obj;
        return id == that.id;
    }

    @Override
    public String toString() {
        return  "id = " + id +
                ", name = '" + name + '\'';
    }
}
