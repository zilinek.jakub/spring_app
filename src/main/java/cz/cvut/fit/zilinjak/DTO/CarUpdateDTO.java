package cz.cvut.fit.zilinjak.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CarUpdateDTO {
    @JsonProperty("color")
    private final String color;
    @JsonProperty("price")
    private final int price;
    @JsonProperty("stk")
    private final boolean stk;
    @JsonProperty("category")
    private final int category_id;
    @JsonProperty("fuel")
    private final String fuel_type;
    @JsonProperty("model")
    private final int model_id;
    @JsonProperty("driven")
    private final int driven;


    @JsonCreator
    public CarUpdateDTO(String color, int price, boolean stk, String fuel_type, int model_id, int category_id, int driven) {
        this.color = color;
        this.price = price;
        this.stk = stk;
        this.fuel_type = fuel_type;
        this.model_id = model_id;
        this.category_id = category_id;
        this.driven = driven;
    }

    public int getDriven() {
        return driven;
    }

    public String getColor() {
        return color;
    }

    public int getPrice() {
        return price;
    }

    public boolean isStk() {
        return stk;
    }

    public int getCategory_id() {
        return category_id;
    }

    public String getFuel_type() {
        return fuel_type;
    }

    public int getModel_id() {
        return model_id;
    }
}
