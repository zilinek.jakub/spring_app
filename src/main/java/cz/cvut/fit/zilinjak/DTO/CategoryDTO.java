package cz.cvut.fit.zilinjak.DTO;


import cz.cvut.fit.zilinjak.entity.Category;

public class CategoryDTO {
    private final int id;
    private final String name;

    public CategoryDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }
    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        CategoryDTO that = (CategoryDTO) obj;
        return id == that.id;
    }

    @Override
    public String toString() {
        return "id = " + id +
                ", name = '" + name + '\'';
    }
}
