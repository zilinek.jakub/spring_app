package cz.cvut.fit.zilinjak.DTO;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TypeCreateDTO {

    @JsonProperty("name")
    private final String name;

    @JsonCreator
    public TypeCreateDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
