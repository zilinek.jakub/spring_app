package cz.cvut.fit.zilinjak.DTO;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CategoryCreateDTO {

    @JsonProperty("name")
    private final String name;

    @JsonCreator
    public CategoryCreateDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
