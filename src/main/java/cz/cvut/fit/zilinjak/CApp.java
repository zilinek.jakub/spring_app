package cz.cvut.fit.zilinjak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class CApp {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(CApp.class);
        app.setDefaultProperties(Collections
        .singletonMap("server.port", "8085"));;
        app.run();
    }
}
