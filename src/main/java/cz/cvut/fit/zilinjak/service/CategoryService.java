package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.CategoryCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CategoryDTO;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    private final CategoryRepository categoryService;

    @Autowired
    public CategoryService(CategoryRepository categoryService) {
        this.categoryService = categoryService;
    }

    /** ============ DTO ============*/
    public List<CategoryDTO> findByIdsDTO(List<Integer> toFind){
        return categoryService.findAllById(toFind).stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public List<CategoryDTO> findAllDTO(){
        return categoryService.findAll().stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public Optional<CategoryDTO> findByIdDTO(int id){
        return toDTO(categoryService.findById(id));
    }

    /** ============ NoDTO ============*/
    public List<Category> findByIds(List<Integer> toFind){
        return categoryService.findAllById(toFind);
    }
    public List<Category> findAll(){
        return categoryService.findAll();
    }
    public Optional<Category> findById(int id){
        return categoryService.findById(id);
    }

    /** ======== Create&Update ========*/
    public CategoryDTO create (CategoryCreateDTO categoryCreateDTO){
        return toDTO( categoryService.save(new Category (categoryCreateDTO.getName())));
    }
    @Transactional
    public CategoryDTO update(int id, CategoryCreateDTO toUpdate) throws Exception {
        Optional<Category> tmp = categoryService.findById(id);
        if (tmp.isEmpty())
            throw new Exception("Invalid ID to update.");
        Category found = tmp.get();
        found.setName(toUpdate.getName());
        return toDTO(found);
    }

    public void delete(int id) throws Exception {
        Optional<Category> toDelete = categoryService.findById(id);
        if (toDelete.isEmpty())
            throw new Exception("Invalid ID to delete.");
        categoryService.delete(toDelete.get());
    }

    /** ============ getDTO ============*/
    private CategoryDTO toDTO(Category category){
        return new CategoryDTO( category.getId() , category.getName() ); // todo
    }
    private Optional<CategoryDTO> toDTO(Optional<Category> category){
        if (category.isEmpty())
            return Optional.empty();
        else
            return Optional.of( toDTO( category.get() ) );
    }
}
