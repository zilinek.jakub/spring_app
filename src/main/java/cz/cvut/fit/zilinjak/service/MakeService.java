package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.MakeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.MakeDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.repository.MakeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MakeService {
    private final MakeRepository makeRepository;
    @Autowired
    public MakeService(MakeRepository makeRepository) {
        this.makeRepository = makeRepository;
    }
    /** ============ DTO ============*/
    public List<MakeDTO> findByIdsDTO(List<Integer> toFind){
        return makeRepository.findAllById(toFind).stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public List<MakeDTO> findAllDTO(){
        return makeRepository.findAll().stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public Optional<MakeDTO> findByIdDTO(int id){
        return toDTO(makeRepository.findById(id));
    }
    /** ============ NoDTO ============*/
    public List<Make> findByIds(List<Integer> toFind){
        return makeRepository.findAllById(toFind);
    }
    public List<Make> findAll(){
        return makeRepository.findAll();
    }
    public Optional<Make> findById(int id){
        return makeRepository.findById(id);
    }
    /** ======== Create&Update ========*/
    public MakeDTO create (MakeCreateDTO makeCreateDTO){
        return toDTO( makeRepository.save(new Make (makeCreateDTO.getName() , makeCreateDTO.getPlace())));
    }
    @Transactional
    public MakeDTO update(int id, MakeCreateDTO toUpdate) throws Exception {
        Optional<Make> tmp = makeRepository.findById(id);
        if (tmp.isEmpty())
            throw new Exception("Invalid ID to update.");
        Make found = tmp.get();
        found.setName(toUpdate.getName());
        found.setPlace(toUpdate.getPlace());
        return toDTO(found);
    }
    public void delete(int id) throws Exception {
        Optional<Make> toDelete = makeRepository.findById(id);
        if (toDelete.isEmpty())
            throw new Exception("Invalid ID to delete.");
        makeRepository.delete(toDelete.get());
    }
    /** ============ getDTO ============*/
    private MakeDTO toDTO(Make make){
        return new MakeDTO( make.getId() , make.getName() ,make.getPlace() ); // todo
    }
    private Optional<MakeDTO> toDTO(Optional<Make> OptionalMake){
        if (OptionalMake.isEmpty())
            return Optional.empty();
        else
            return Optional.of( toDTO( OptionalMake.get() ) );
    }
}
