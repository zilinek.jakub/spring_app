package cz.cvut.fit.zilinjak.service;


import cz.cvut.fit.zilinjak.DTO.*;
import cz.cvut.fit.zilinjak.entity.*;
import cz.cvut.fit.zilinjak.repository.CarRepository;
import cz.cvut.fit.zilinjak.repository.CategoryRepository;
import cz.cvut.fit.zilinjak.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CarService {
    private final ModelRepository modelRepository;
    private final CategoryRepository categoryRepository;
    private final CarRepository carRepository;

    @Autowired
    public CarService(ModelRepository modelRepository, CategoryRepository categoryRepository, CarRepository carRepository) {
        this.modelRepository = modelRepository;
        this.categoryRepository = categoryRepository;
        this.carRepository = carRepository;
    }

    /** ============ DTO ============*/
    public Optional<CarDTO> findByIdDTO(String id){
        return toDTO(carRepository.findById(id));
    }
    public List<CarDTO> findAllDTO(){
        return carRepository.findAll().stream()
                .map(this::toDTO).collect(Collectors.toList());
    }

    /** ============ NoDTO ============*/
    public Optional<Car> findById(String id){
        return carRepository.findById(id);
    }
    public List<Car> findAll(){
        return carRepository.findAll();
    }

    /** ======== Create&Update ========*/
    public CarDTO create(CarCreateDTO toCreate) throws Exception {
        Optional<Model> modelCheck = modelRepository.findById(toCreate.getModel_id());
        Optional<Category> categoryCheck = categoryRepository.findById(toCreate.getCategory_id());

        if (categoryCheck.isEmpty())
            throw new Exception("Invalid Category ID.");
        if ( modelCheck.isEmpty() )
            throw new Exception("Invalid Model ID");

        return toDTO(carRepository.save (
                new Car(toCreate.getVin() ,toCreate.getColor() , toCreate.getPrice() ,
                        toCreate.isStk() , toCreate.getFuel_type() , modelCheck.get() ,
                        toCreate.getDriven() , categoryCheck.get())
        ));
    }
    @Transactional
    public CarDTO update(String id, CarUpdateDTO toUpdate) throws  Exception{

        Optional<Model> checkModel = modelRepository.findById(toUpdate.getModel_id());
        Optional<Car> checkCar = carRepository.findById(id);
        Optional<Category> checkCategory = categoryRepository.findById(toUpdate.getCategory_id());

        if (checkModel.isEmpty())
            throw new Exception("Invalid Model ID.");
        if ( checkCar.isEmpty() )
            throw new Exception("Invalid Car ID");
        if ( checkCategory.isEmpty())
            throw  new Exception("Invalid Category ID.");
        Car target = checkCar.get();

        target.setDriven(toUpdate.getDriven());
        target.setCategory(checkCategory.get());
        target.setModel(checkModel.get());
        target.setColor(toUpdate.getColor());
        target.setFuel_type(toUpdate.getFuel_type());
        target.setStk(toUpdate.isStk());
        target.setPrice(toUpdate.getPrice());
        return toDTO(target);
    }

    public void delete(String id) throws Exception {
        Optional<Car> toDelete = carRepository.findById(id);
        if (toDelete.isEmpty())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        carRepository.delete(toDelete.get());
    }

    /** ============ getDTO ============*/
    private CarDTO toDTO(Car car){
         return new CarDTO( car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,
                            car.getFuel_type() , car.getModel().getId() , car.getCategory().getId(), car.getDriven());
    }
    private Optional<CarDTO> toDTO(Optional<Car> OptionalCar){
        if (OptionalCar.isEmpty())
            return Optional.empty();
        else
            return Optional.of( toDTO( OptionalCar.get()) );
    }
}
