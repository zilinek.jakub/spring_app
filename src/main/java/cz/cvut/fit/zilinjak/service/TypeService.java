package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TypeService {
    private final TypeRepository typeRepository;

    @Autowired
    public TypeService(TypeRepository typeRepository) {
        this.typeRepository = typeRepository;
    }

    /** ============ DTO ============*/
    public List<TypeDTO> findByIdsDTO(List<Integer> toFind){
        return typeRepository.findAllById(toFind).stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public List<TypeDTO> findAllDTO(){
        return typeRepository.findAll().stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public Optional<TypeDTO> findByIdDTO(int id){
        return toDTO(typeRepository.findById(id));
    }

    /** ============ NoDTO ============*/
    public List<Type> findByIds(List<Integer> toFind){
        return typeRepository.findAllById(toFind);
    }
    public List<Type> findAll(){
        return typeRepository.findAll();
    }
    public Optional<Type> findById(int id){
        return typeRepository.findById(id);
    }

    /** ======== Create&Update ========*/
    public TypeDTO create (TypeCreateDTO typeCreateDTO){
        return toDTO(typeRepository.save( new Type( typeCreateDTO.getName() ) ) );
    }
    @Transactional
    public TypeDTO update(int id, TypeCreateDTO toUpdate) throws Exception {
        Optional<Type> tmp = typeRepository.findById(id);
        if (tmp.isEmpty())
            throw new Exception("Invalid ID of Type to update.");
        Type found = tmp.get();
        found.setName(toUpdate.getName());
        return toDTO(found);
    }

    public void delete(int id) throws Exception {
        Optional<Type> toDelete = typeRepository.findById(id);
        if (toDelete.isEmpty())
            throw new Exception("Invalid ID.");
        typeRepository.delete(toDelete.get());
    }

    /** ============ getDTO ============*/
    private TypeDTO toDTO(Type Type){
        return new TypeDTO( Type.getId() , Type.getName() );
    }
    private Optional<TypeDTO> toDTO(Optional<Type> optionalType){
        if (optionalType.isEmpty())
            return Optional.empty();
        else
            return Optional.of( toDTO( optionalType.get() ) );
    }
}
