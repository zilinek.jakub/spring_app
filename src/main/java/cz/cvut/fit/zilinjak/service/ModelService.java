package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.ModelCreateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Model;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.MakeRepository;
import cz.cvut.fit.zilinjak.repository.ModelRepository;
import cz.cvut.fit.zilinjak.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ModelService {
    private final TypeRepository typeRepository;
    private final ModelRepository modelRepository;

    private final MakeService makeService;

    @Autowired
    public ModelService(TypeRepository typeRepository, ModelRepository modelRepository, MakeService makeService) {
        this.typeRepository = typeRepository;
        this.modelRepository = modelRepository;
        this.makeService = makeService;
    }

    /** ============ DTO ============*/
    public List<ModelDTO> findByIdsDTO(List<Integer> toFind){
        return modelRepository.findAllById(toFind).stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public List<ModelDTO> findAllDTO(){
        return modelRepository.findAll().stream()
                .map(this::toDTO).collect(Collectors.toList());
    }
    public Optional<ModelDTO> findByIdDTO(int id){
        return toDTO(modelRepository.findById(id));
    }
    public List<ModelDTO> findByType(String typeName){
        return modelRepository.findByType(typeName).stream().map(this::toDTO).collect(Collectors.toList());
    }

    /** ============ NoDTO ============*/
    public List<Model> findByIds(List<Integer> toFind){
        return modelRepository.findAllById(toFind);
    }
    public List<Model> findAll(){
        return modelRepository.findAll();
    }
    public Optional<Model> findById(int id){
        return modelRepository.findById(id);
    }
    //TODO napr stk and date
    /** ======== Create&Update ========*/
    public ModelDTO create(ModelCreateDTO toCreate) throws Exception {
        Optional<Type> checkType = typeRepository.findById(toCreate.getType_id());
        List<Make> makers = makeService.findByIds(toCreate.getMake_id());

        if (checkType.isEmpty() || makers.size() != toCreate.getMake_id().size())
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

        return toDTO(modelRepository.save ( new Model(toCreate.getName() , toCreate.getDate(),
                                                      checkType.get() , makers ) ));
    }
    @Transactional
    public ModelDTO update(int id, ModelCreateDTO toUpdate) throws  Exception{
        Optional<Type> checkType = typeRepository.findById(toUpdate.getType_id());
        Optional<Model> checkModel = modelRepository.findById(id);
        List<Make> makers = makeService.findByIds(toUpdate.getMake_id());

        if (makers.size() != toUpdate.getMake_id().size() )
            throw new Exception("invalid ID of maker.");
        if (checkType.isEmpty())
            throw new Exception("Invalid Type ID.");
        if (checkModel.isEmpty())
            throw new Exception("Invalid Model ID");

        Model target = checkModel.get();
        target.setDate(toUpdate.getDate());
        target.setName(toUpdate.getName());
        target.setMake(makers);
        target.setType(checkType.get());
        return toDTO(target);
    }

    public void delete(int id) throws Exception {
        Optional<Model> toDelete = modelRepository.findById(id);
        if (toDelete.isEmpty())
            throw new Exception("Invalid ID to delete.");
        modelRepository.delete(toDelete.get());
    }
    /** ============ getDTO ============*/
    private ModelDTO toDTO(Model model){
        return new ModelDTO( model.getId() , model.getName() , model.getDate() ,
                             model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));
    }
    private Optional<ModelDTO> toDTO(Optional<Model> OptionalModel){
        if (OptionalModel.isEmpty())
            return Optional.empty();
        else
            return Optional.of( toDTO( OptionalModel.get() ) );
    }
}





















