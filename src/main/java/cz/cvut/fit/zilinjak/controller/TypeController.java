package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/type")
public class TypeController {

    private final TypeService typeService;

    @Autowired
    public TypeController(TypeService typeService) {
        this.typeService = typeService;
    }
    /** ============ DTO ============*/
    @GetMapping("/all")
    List<TypeDTO> allDTO(){
        return typeService.findAllDTO();
    }
    @GetMapping("/{id}/{id2}")
    List<TypeDTO> findByIdsDTO(@PathVariable int id , @PathVariable int id2 ){
        List<Integer> ids = new ArrayList<>();
        for (int i = id ; i <= id2 ; i++ )
            ids.add(i);
        return typeService.findByIdsDTO(ids);
    }
    @GetMapping("/{id}")
    TypeDTO byIdDTO(@PathVariable int id){
        return typeService.findByIdDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /** ======== Create&Update ========*/
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/create")     // name
    TypeDTO save(@RequestBody TypeCreateDTO typeCreateDTO) {
        return typeService.create( typeCreateDTO );
    }
    @PutMapping("/update/{id}")
    TypeDTO update(@PathVariable int id, @RequestBody TypeCreateDTO typeCreateDTO){
        try {
            return typeService.update(id, typeCreateDTO);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable int id){
        try {
            typeService.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    //    /** ============ NoDTO ============*/
//    @GetMapping("/all")
//    List<Type> all(){
//        return typeService.findAll();
//    }
//    @GetMapping("/{id}/{id2}")
//    List<Type> findByIds(@PathVariable int id , @PathVariable int id2 ){
//        List<Integer> ids = new ArrayList<>();
//        for (int i = id ; i <= id2 ; i++ )
//            ids.add(i);
//        return typeService.findByIds(ids);
//    }
//    @GetMapping("/{id}")
//    Type byId(@PathVariable int id){
//        return typeService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
//
}
