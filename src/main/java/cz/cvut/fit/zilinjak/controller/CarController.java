package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.CarCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CarDTO;
import cz.cvut.fit.zilinjak.DTO.CarUpdateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelCreateDTO;
import cz.cvut.fit.zilinjak.entity.Car;
import cz.cvut.fit.zilinjak.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarController {
    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }
    /** ============ DTO ============*/
    @GetMapping("/all")
    List<CarDTO> allDTO(){
        return carService.findAllDTO();
    }
    @GetMapping("/{id}")
    CarDTO findByIdDTO(@PathVariable String id){
        return carService.findByIdDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }


    @GetMapping("/{id1}/{id2}")
    int About (@PathVariable int id1, @PathVariable int id2){
        return id1 + id2;
    }

    /** ======== Create&Update ========*/
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    CarDTO create(@RequestBody CarCreateDTO carCreateDTO){
        try {
            return carService.create( carCreateDTO );
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("/update/{id}")
    CarDTO save(@PathVariable String id, @RequestBody CarUpdateDTO carUpdateDTO){
        try {
            return carService.update(id, carUpdateDTO);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable String id){
        try {
            carService.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }





    //    /** ============ NoDTO ============*/
//    @GetMapping("/all")
//    List<Car> all(){
//        return carService.findAll();
//    }
//    @GetMapping("/{id1}")
//    Car findById(@PathVariable String id1){
//        return carService.findById(id1).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
}
