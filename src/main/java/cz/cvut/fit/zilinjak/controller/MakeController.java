package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.MakeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.MakeDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.service.MakeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/make")
public class MakeController {

    private final MakeService makeService;

    @Autowired
    public MakeController(MakeService makeService) {
        this.makeService = makeService;
    }
    /** ============ DTO ============*/
    @GetMapping("/all")
    List<MakeDTO> allDTO(){
        return makeService.findAllDTO();
    }
    @GetMapping("/{id}/{id2}")
    List<MakeDTO> findByIdsDTO(@PathVariable int id , @PathVariable int id2 ){
        List<Integer> ids = new ArrayList<>();
        for (int i = id ; i <= id2 ; i++ )
            ids.add(i);
        return makeService.findByIdsDTO(ids);
    }
    @GetMapping("/{id}")
    MakeDTO byIdDTO(@PathVariable int id){
        return makeService.findByIdDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /** ======== Create&Update ========*/
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    MakeDTO create(@RequestBody MakeCreateDTO makeCreateDTO) {
         return makeService.create( makeCreateDTO );
    }

    @PutMapping("/update/{id}")
    MakeDTO save(@PathVariable int id, @RequestBody MakeCreateDTO make){
        try {
            return makeService.update(id, make);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable int id){
        try {
            makeService.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

//    /** ============ NoDTO ============*/
//    @GetMapping("/all")
//    List<Make> all(){
//        return makeService.findAll();
//    }
//    @GetMapping("/{id}/{id2}")
//    List<Make> findByIds(@PathVariable int id , @PathVariable int id2 ){
//        List<Integer> ids = new ArrayList<>();
//        for (int i = id ; i <= id2 ; i++ )
//            ids.add(i);
//        return makeService.findByIds(ids);
//    }
//    @GetMapping("/{id}")
//    Make byId(@PathVariable int id){
//        return makeService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
}
