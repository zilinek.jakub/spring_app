package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.CategoryCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CategoryDTO;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /** ============ DTO ============*/
    @GetMapping("/all")
    List<CategoryDTO> allDTO(){
        return categoryService.findAllDTO();
    }
    @GetMapping("/{id}/{id2}")
    List<CategoryDTO> findByIdsDTO(@PathVariable int id , @PathVariable int id2){
        List<Integer> ids = new ArrayList<>();
        for (int i = id ; i <= id2 ; i++ )
            ids.add(i);
        return categoryService.findByIdsDTO(ids);
    }
    @GetMapping("/{id}")
    CategoryDTO byIdDTO(@PathVariable int id){
        return categoryService.findByIdDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    /** ======== Create&Update ========*/
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    CategoryDTO create(@RequestBody CategoryCreateDTO categoryCreateDTO) {
        return categoryService.create( categoryCreateDTO );
    }
    @PutMapping("/update/{id}")
    CategoryDTO save(@PathVariable int id, @RequestBody CategoryCreateDTO category){
        try {
            return categoryService.update(id, category);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable int id){
        try {
            categoryService.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }




//    /** ============ NoDTO ============*/
//    @GetMapping("/all")
//    List<Category> all(){
//        return categoryService.findAll();
//    }
//    @GetMapping("/{id}/{id2}")
//    List<Category> findByIds(@PathVariable int id , @PathVariable int id2 ){
//        List<Integer> ids = new ArrayList<>();
//        for (int i = id ; i <= id2 ; i++ )
//            ids.add(i);
//        return categoryService.findByIds(ids);
//    }
//    @GetMapping("/{id}")
//    Category byId(@PathVariable int id){
//        return categoryService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
}
