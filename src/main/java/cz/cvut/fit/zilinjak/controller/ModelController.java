package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.ModelCreateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelDTO;
import cz.cvut.fit.zilinjak.entity.Model;
import cz.cvut.fit.zilinjak.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/model")
public class ModelController {

    private final ModelService modelService;

    @Autowired
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    /** ============ DTO ============*/
    @GetMapping("/all")
    List<ModelDTO> allDTO(){
        return modelService.findAllDTO();
    }
    @GetMapping("/{id}/{id2}")
    List<ModelDTO> findByIdsDTO(@PathVariable int id , @PathVariable int id2 ){
        List<Integer> ids = new ArrayList<>();
        for (int i = id ; i <= id2 ; i++ )
            ids.add(i);
        return modelService.findByIdsDTO(ids);
    }
    @GetMapping("/{id}")
    ModelDTO byIdDTO(@PathVariable int id){
        return modelService.findByIdDTO(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
    @GetMapping("/byType/{typeName}")
    List<ModelDTO> byTypeName(@PathVariable String typeName){
        return modelService.findByType(typeName);
    }
    /** ======== Create&Update ========*/
    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    ModelDTO create(@RequestBody ModelCreateDTO modelCreateDTO){
        try {
            return modelService.create( modelCreateDTO );
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("/update/{id}")
    ModelDTO save(@PathVariable int id, @RequestBody ModelCreateDTO modelCreateDTO){
        try {
            return modelService.update(id, modelCreateDTO);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/delete/{id}")
    void delete(@PathVariable int id){
        try {
            modelService.delete(id);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


//    /** ============ NoDTO ============*/
//    @GetMapping("/all")
//    List<Model> all(){
//        return modelService.findAll();
//    }
//    @GetMapping("/{id}/{id2}")
//    List<Model> findByIds(@PathVariable int id , @PathVariable int id2 ){
//        List<Integer> ids = new ArrayList<>();
//        for (int i = id ; i <= id2 ; i++ )
//            ids.add(i);
//        return modelService.findByIds(ids);
//    }
//    @GetMapping("/{id}")
//    Model byId(@PathVariable int id){
//        return modelService.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
//    }
}
