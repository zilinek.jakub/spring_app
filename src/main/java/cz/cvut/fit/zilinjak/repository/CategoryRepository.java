package cz.cvut.fit.zilinjak.repository;

import cz.cvut.fit.zilinjak.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<Category , Integer> {
}
