package cz.cvut.fit.zilinjak.repository;

import cz.cvut.fit.zilinjak.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, String> {
}
