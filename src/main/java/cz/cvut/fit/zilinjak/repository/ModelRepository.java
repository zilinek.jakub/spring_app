package cz.cvut.fit.zilinjak.repository;

import cz.cvut.fit.zilinjak.entity.Model;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<Model,Integer> {

    @Query(
            "Select model From Model model " +
                    "join Type type on (model.type.id = type.id) " +
                    "where :typeName = type.name"
    )
    List<Model> findByType(String typeName);

}
