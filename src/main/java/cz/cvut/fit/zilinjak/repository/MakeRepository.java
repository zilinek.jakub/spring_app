package cz.cvut.fit.zilinjak.repository;

import cz.cvut.fit.zilinjak.entity.Make;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MakeRepository extends JpaRepository<Make,Integer> {
}
