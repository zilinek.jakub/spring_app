package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.service.TypeService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@AutoConfigureMockMvc
@SpringBootTest
class TypeControllerTest {

    @MockBean
    private TypeService service;

    @Autowired
    private MockMvc mock;

    @Test
    void all() throws Exception {
        Type type1 = new Type("TypeName1");
        TypeDTO typeDTO1 = new TypeDTO( type1.getId(),type1.getName() );
        Type type2 = new Type("TypeName2");
        TypeDTO typeDTO2 = new TypeDTO( type2.getId(),type2.getName() );
        Type type3 = new Type("TypeName3");
        TypeDTO typeDTO3 = new TypeDTO( type3.getId(),type3.getName() );
        List<TypeDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));

        BDDMockito.given(service.findAllDTO()).willReturn(list);


        mock.perform(MockMvcRequestBuilders.get("/api/type/all")
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())));
    }

    @Test
    void findByIds() throws Exception {
        Type type1 = new Type("TypeName1");
        TypeDTO typeDTO1 = new TypeDTO( type1.getId(),type1.getName() );
        Type type2 = new Type("TypeName2");
        TypeDTO typeDTO2 = new TypeDTO( type2.getId(),type2.getName() );
        Type type3 = new Type("TypeName3");
        TypeDTO typeDTO3 = new TypeDTO( type3.getId(),type3.getName() );
        List<TypeDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));
        List<Integer> ints = new ArrayList<>();

        for (int i = 0 ; i <= 2 ; i++) ints.add(i);
        BDDMockito.given(service.findByIdsDTO(ints)).willReturn(list);
        String toSend  = "/api/type/0/2";
        mock.perform(MockMvcRequestBuilders.get(toSend)
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())))
        ;
    }

    @Test
    void byId() throws Exception {
        Type type = new Type("TypeName");
        TypeDTO typeDTO = new TypeDTO( type.getId(),"TypeName" );

        BDDMockito.given(service.findByIdDTO(type.getId())).willReturn(Optional.of(typeDTO));

        mock.perform(MockMvcRequestBuilders.get("/api/type/" + type.getId())
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(type.getName()))
                );
        Mockito.verify(service , Mockito.atLeastOnce()).findByIdDTO(type.getId());
    }

    @Test
    void save() throws Exception {
        Type type = new Type("TypeName");
        TypeDTO typeDTO = new TypeDTO( type.getId(),"TypeName" );
        TypeCreateDTO CreateDTO = new TypeCreateDTO( "TypeName2" );

        BDDMockito.given(service.create(Mockito.any(TypeCreateDTO.class))).willReturn(typeDTO);

        String toSend = "{ \"name\": \"" + CreateDTO.getName() + "\" }";

        mock.perform(MockMvcRequestBuilders.post("/api/type/create/")
                .contentType("application/json")
                .accept("application/json")
                .content(toSend))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(type.getName())))
        ;
        Mockito.verify(service , Mockito.atLeastOnce()).create(Mockito.any(TypeCreateDTO.class));
    }

    @Test
    void update() throws Exception {
        Type type = new Type("TypeName");
        TypeDTO typeDTO = new TypeDTO( type.getId(),"TypeName" );

        BDDMockito.given(service.update(Mockito.anyInt(), Mockito.any(TypeCreateDTO.class))).willReturn(typeDTO);
        String toSend = "{ \"name\": \"TypeName2\" }";

        mock.perform(MockMvcRequestBuilders.put("/api/type/update/" + type.getId())
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(type.getName())));
        Mockito.verify(service , Mockito.atLeastOnce()).update(Mockito.anyInt(), Mockito.any(TypeCreateDTO.class));
    }

    @Test
    void delete() throws Exception {
        Type type = new Type("TypeName");
        mock.perform(MockMvcRequestBuilders.delete("/api/type/delete/" + type.getId())
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service , Mockito.atLeastOnce()).delete(type.getId());
    }
}