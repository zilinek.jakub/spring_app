package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.MakeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.MakeDTO;
import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.service.MakeService;
import cz.cvut.fit.zilinjak.service.TypeService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
class MakeControllerTest {

    @MockBean
    private MakeService service;

    @Autowired
    private MockMvc mock;

    @Test
    void allDTO() throws Exception {
        Make type1 = new Make("MakeName1", "Place1");
        Make type2 = new Make("MakeName2", "Place2");
        Make type3 = new Make("MakeName3", "Place3");
        MakeDTO typeDTO1 = new MakeDTO( type1.getId(),type1.getName() , type1.getPlace() );
        MakeDTO typeDTO2 = new MakeDTO( type2.getId(),type2.getName() , type2.getPlace());
        MakeDTO typeDTO3 = new MakeDTO( type3.getId(),type3.getName() , type3.getPlace());


        List<MakeDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));

        BDDMockito.given(service.findAllDTO()).willReturn(list);

        mock.perform(MockMvcRequestBuilders.get("/api/make/all")
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].place", CoreMatchers.is(type1.getPlace())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].place", CoreMatchers.is(type2.getPlace())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].place", CoreMatchers.is(type3.getPlace())));


    }

    @Test
    void findByIdsDTO() throws Exception {

        Make type1 = new Make("MakeName1", "Place1");
        Make type2 = new Make("MakeName2", "Place2");
        Make type3 = new Make("MakeName3", "Place3");
        MakeDTO typeDTO1 = new MakeDTO( type1.getId(),type1.getName() , type1.getPlace() );
        MakeDTO typeDTO2 = new MakeDTO( type2.getId(),type2.getName() , type2.getPlace());
        MakeDTO typeDTO3 = new MakeDTO( type3.getId(),type3.getName() , type3.getPlace());

        
        List<MakeDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));
        List<Integer> ints = new ArrayList<>();
        for (int i = 0 ; i <= 2 ; i++) ints.add(i);
        BDDMockito.given(service.findByIdsDTO(ints)).willReturn(list);
        mock.perform(MockMvcRequestBuilders.get("/api/make/0/2")
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].place", CoreMatchers.is(type1.getPlace())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].place", CoreMatchers.is(type2.getPlace())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].place", CoreMatchers.is(type3.getPlace())))
        ;


    }

    @Test
    void byIdDTO() throws Exception {
        Make make = new Make("name" , "place");
        MakeDTO makeDTO = new MakeDTO(make.getId() , make.getName() , make.getPlace());

        BDDMockito.given(service.findByIdDTO(make.getId())).willReturn(Optional.of(makeDTO));

        mock.perform(MockMvcRequestBuilders.get("/api/make/" + make.getId())
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(make.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.place", CoreMatchers.is(make.getPlace()))
                );
        Mockito.verify(service , Mockito.atLeastOnce()).findByIdDTO(make.getId());
    }

    @Test
    void create() throws Exception {
        Make make = new Make("name" , "place");
        MakeDTO makeDTO = new MakeDTO(make.getId() , make.getName() , make.getPlace());

        BDDMockito.given(service.create(Mockito.any(MakeCreateDTO.class))).willReturn(makeDTO);
        String toSend = "{ \"name\": \"name\", \"place\": \"place\" }";

        mock.perform(MockMvcRequestBuilders.post("/api/make/create/")
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(makeDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.place", CoreMatchers.is(makeDTO.getPlace())));;
        Mockito.verify(service , Mockito.atLeastOnce()).create(Mockito.any(MakeCreateDTO.class));
    }

    @Test
    void save() throws Exception {
        Make make = new Make("name" , "place");
        MakeDTO makeDTO = new MakeDTO(make.getId() , make.getName() , make.getPlace());

        BDDMockito.given(service.update(Mockito.anyInt(), Mockito.any(MakeCreateDTO.class))).willReturn(makeDTO);
        String toSend = "{ \"name\": \"name\", \"place\": \"place2\" }";

        mock.perform(MockMvcRequestBuilders.put("/api/make/update/" + make.getId())
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(makeDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.place", CoreMatchers.is(makeDTO.getPlace())));
        Mockito.verify(service , Mockito.atLeastOnce()).update(Mockito.anyInt(), Mockito.any(MakeCreateDTO.class));
    }

    @Test
    void delete() throws Exception {
        Make make = new Make("name" , "place");
        mock.perform(MockMvcRequestBuilders.delete("/api/make/delete/" + make.getId())
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service , Mockito.atLeastOnce()).delete(make.getId());
    }
}