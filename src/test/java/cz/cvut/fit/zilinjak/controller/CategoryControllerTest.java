package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.*;
import cz.cvut.fit.zilinjak.entity.*;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.service.CategoryService;
import cz.cvut.fit.zilinjak.service.CategoryService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


@AutoConfigureMockMvc
@SpringBootTest
class CategoryControllerTest {

    @MockBean
    private CategoryService service;

    @Autowired
    private MockMvc mock;

    @Test
    void allDTO() throws Exception {
        Category type1 = new Category("CategoryName1");
        Category type2 = new Category("CategoryName2");
        Category type3 = new Category("CategoryName3");
        CategoryDTO typeDTO1 = new CategoryDTO( type1.getId(),type1.getName());
        CategoryDTO typeDTO2 = new CategoryDTO( type2.getId(),type2.getName());
        CategoryDTO typeDTO3 = new CategoryDTO( type3.getId(),type3.getName());


        List<CategoryDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));
        BDDMockito.given(service.findAllDTO()).willReturn(list);
        mock.perform(MockMvcRequestBuilders.get("/api/category/all")
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())))
        ;
    }

    @Test
    void findByIdsDTO() throws Exception {
        Category type1 = new Category("CategoryName1");
        Category type2 = new Category("CategoryName2");
        Category type3 = new Category("CategoryName3");
        CategoryDTO typeDTO1 = new CategoryDTO( type1.getId(),type1.getName());
        CategoryDTO typeDTO2 = new CategoryDTO( type2.getId(),type2.getName());
        CategoryDTO typeDTO3 = new CategoryDTO( type3.getId(),type3.getName());


        List<CategoryDTO> list = new ArrayList<>(List.of(typeDTO1 , typeDTO2 , typeDTO3));
        List<Integer> ints = new ArrayList<>();
        for (int i = 0 ; i <= 2 ; i++) ints.add(i);
        BDDMockito.given(service.findByIdsDTO(ints)).willReturn(list);
        mock.perform(MockMvcRequestBuilders.get("/api/category/0/2")
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(type1.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(type2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].name", CoreMatchers.is(type3.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(type1.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(type2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.is(type3.getId())))
        ;


    }

    @Test
    void byIdDTO() throws Exception {
        Category Category = new Category("CategoryName");
        CategoryDTO CategoryDTO = new CategoryDTO( Category.getId(),"CategoryName" );

        BDDMockito.given(service.findByIdDTO(Category.getId())).willReturn(Optional.of(CategoryDTO));

        mock.perform(MockMvcRequestBuilders.get("/api/category/" + Category.getId())
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(Category.getName()))
                );
        Mockito.verify(service , Mockito.atLeastOnce()).findByIdDTO(Category.getId());
    }

    @Test
    void create() throws Exception {
        Category Category = new Category("CategoryName");
        CategoryDTO CategoryDTO = new CategoryDTO( Category.getId(),"CategoryName" );
        CategoryCreateDTO CreateDTO = new CategoryCreateDTO( "CategoryName2" );

        BDDMockito.given(service.create(Mockito.any(CategoryCreateDTO.class))).willReturn(CategoryDTO);

        String toSend = "{ \"name\": \"" + CreateDTO.getName() + "\" }";

        mock.perform(MockMvcRequestBuilders.post("/api/category/create")
                .contentType("application/json")
                .accept("application/json")
                .content(toSend))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(CategoryDTO.getName())));
        Mockito.verify(service , Mockito.atLeastOnce()).create(Mockito.any(CategoryCreateDTO.class));
    }

    @Test
    void save() throws Exception {
        Category Category = new Category("CategoryName");
        CategoryDTO CategoryDTO = new CategoryDTO( Category.getId(),"CategoryName" );
        CategoryCreateDTO CreateDTO = new CategoryCreateDTO( "CategoryName2" );

        BDDMockito.given(service.update(Mockito.anyInt(), Mockito.any(CategoryCreateDTO.class))).willReturn(CategoryDTO);
        String toSend = "{ \"name\": \"CategoryName2\" }";

        mock.perform(MockMvcRequestBuilders.put("/api/category/update/" + Category.getId())
                .contentType("application/json")
                .accept("application/json")
                .content(toSend))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(CategoryDTO.getName())));
        Mockito.verify(service , Mockito.atLeastOnce()).update(Mockito.anyInt(), Mockito.any(CategoryCreateDTO.class));
    }

    @Test
    void delete() throws Exception {
        Category Category = new Category("name");
        mock.perform(MockMvcRequestBuilders.delete("/api/category/delete/" + Category.getId())
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service , Mockito.atLeastOnce()).delete(Category.getId());
    }
}