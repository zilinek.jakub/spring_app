package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.CarCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CarDTO;
import cz.cvut.fit.zilinjak.DTO.CarUpdateDTO;
import cz.cvut.fit.zilinjak.DTO.MakeCreateDTO;
import cz.cvut.fit.zilinjak.entity.*;
import cz.cvut.fit.zilinjak.service.CarService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
class CarControllerTest {

    @MockBean
    CarService service;

    @Autowired
    private MockMvc mock;

    private Category cat;
    private Model model;

    @BeforeEach
    void init(){
        Make make = new Make("Makename", "makePlace");
        Type type = new Type("typeName");
        cat = new Category("kategorie");
        model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList(make));
    }

    @Test
    void allDTO() throws Exception {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        Car car2 = new Car("vin2" , "color" , 333 , true ,
                "petrolHead" , model ,0 , cat );
        Car car3 = new Car("vin1" , "color" , 666 , true ,
                "petrolHead" , model ,0 , cat );

        CarDTO carDTO = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,
                car.getFuel_type() , car.getModel().getId() ,car.getCategory().getId() , car.getCategory().getId() );
        CarDTO carDTO2 = new CarDTO(car2.getVin() , car2.getColor() , car2.getPrice() , car2.isStk() ,
                car2.getFuel_type() , car2.getModel().getId() ,car2.getCategory().getId() , car2.getCategory().getId() );
        CarDTO carDTO3 = new CarDTO(car3.getVin() , car3.getColor() , car3.getPrice() , car3.isStk() ,
                car3.getFuel_type() , car3.getModel().getId() ,car3.getCategory().getId() , car3.getCategory().getId() );

        List<CarDTO> list = new ArrayList<>(
                List.of(carDTO , carDTO2 , carDTO3)
        );
        BDDMockito.given(service.findAllDTO()).willReturn(list);
        mock.perform(MockMvcRequestBuilders.get("/api/car/all")
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                // [
                // {"vin":"vin666","color":"color","price":199,"stk":true,"category_id":0,"fuel_type":"petrolHead","model_id":0,"driven":0},
                // {"vin":"vin2","color":"color","price":333,"stk":true,"category_id":0,"fuel_type":"petrolHead","model_id":0,"driven":0},
                // {"vin":"vin1","color":"color","price":666,"stk":true,"category_id":0,"fuel_type":"petrolHead","model_id":0,"driven":0}
                // ]
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].vin", CoreMatchers.is(carDTO.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].color", CoreMatchers.is(carDTO.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].price", CoreMatchers.is(carDTO.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].stk", CoreMatchers.is(carDTO.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].category_id", CoreMatchers.is(carDTO.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].fuel_type", CoreMatchers.is(carDTO.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].model_id", CoreMatchers.is(carDTO.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].driven", CoreMatchers.is(carDTO.getDriven())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].vin", CoreMatchers.is(carDTO2.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].color", CoreMatchers.is(carDTO2.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].price", CoreMatchers.is(carDTO2.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].stk", CoreMatchers.is(carDTO2.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].category_id", CoreMatchers.is(carDTO2.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].fuel_type", CoreMatchers.is(carDTO2.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].model_id", CoreMatchers.is(carDTO2.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].driven", CoreMatchers.is(carDTO2.getDriven())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].vin", CoreMatchers.is(carDTO3.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].color", CoreMatchers.is(carDTO3.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].price", CoreMatchers.is(carDTO3.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].stk", CoreMatchers.is(carDTO3.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].category_id", CoreMatchers.is(carDTO3.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].fuel_type", CoreMatchers.is(carDTO3.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].model_id", CoreMatchers.is(carDTO3.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].driven", CoreMatchers.is(carDTO3.getDriven())))
        ;
    }

    @Test
    void findByIdDTO() throws Exception {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );

        CarDTO carDTO = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,
                car.getFuel_type() , car.getModel().getId() ,car.getCategory().getId() , car.getCategory().getId() );

        BDDMockito.given(service.findByIdDTO(car.getVin())).willReturn(Optional.of(carDTO));
        mock.perform(MockMvcRequestBuilders.get("/api/car/"+car.getVin())
                .contentType("application/json")
                .accept("application/json"))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.vin", CoreMatchers.is(carDTO.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.color", CoreMatchers.is(carDTO.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", CoreMatchers.is(carDTO.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stk", CoreMatchers.is(carDTO.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.category_id", CoreMatchers.is(carDTO.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fuel_type", CoreMatchers.is(carDTO.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.model_id", CoreMatchers.is(carDTO.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.driven", CoreMatchers.is(carDTO.getDriven())))
        ;
    }

    @Test
    void create() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        CarDTO dto = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,car.getFuel_type(),
                car.getModel().getId() , car.getCategory().getId() , car.getDriven());


        BDDMockito.given(service.create(Mockito.any(CarCreateDTO.class))).willReturn(dto);
        String toSend = "{\"vin\": \"vin\", \"color\": \"color\"\t,\"price\": \"199\",  \"driven\": \"0\",  \"stk\": \"true\", \"fuel\": \"petrolHead\"," +
                "      \"model\": \"0\", \"category\":\"0\"}\n";

        mock.perform(MockMvcRequestBuilders.post("/api/car/create")
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.vin", CoreMatchers.is(dto.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.color", CoreMatchers.is(dto.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", CoreMatchers.is(dto.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stk", CoreMatchers.is(dto.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.category_id", CoreMatchers.is(dto.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fuel_type", CoreMatchers.is(dto.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.model_id", CoreMatchers.is(dto.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.driven", CoreMatchers.is(dto.getDriven())))
        ;
        Mockito.verify(service , Mockito.atLeastOnce()).create(Mockito.any(CarCreateDTO.class));
    }

    @Test
    void save() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        CarDTO dto = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,car.getFuel_type(),
                car.getModel().getId() , car.getCategory().getId() , car.getDriven());


        BDDMockito.given(service.update(Mockito.anyString(),Mockito.any(CarUpdateDTO.class))).willReturn(dto);
        String toSend = "{\"vin\": \"vin2\", \"color\": \"color2\"\t,\"price\": \"199\",  \"driven\": \"0\",  \"stk\": \"true\", \"fuel\": \"petrolHead\"," +
                "      \"model\": \"0\", \"category\":\"0\"}\n";

        mock.perform(MockMvcRequestBuilders.put("/api/car/update/"+car.getVin())
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.vin", CoreMatchers.is(dto.getVin())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.color", CoreMatchers.is(dto.getColor())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.price", CoreMatchers.is(dto.getPrice())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.stk", CoreMatchers.is(dto.isStk())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.category_id", CoreMatchers.is(dto.getCategory_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.fuel_type", CoreMatchers.is(dto.getFuel_type())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.model_id", CoreMatchers.is(dto.getModel_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.driven", CoreMatchers.is(dto.getDriven())));
        Mockito.verify(service , Mockito.atLeastOnce()).update(Mockito.anyString(),Mockito.any(CarUpdateDTO.class));

    }

    @Test
    void delete() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        mock.perform(MockMvcRequestBuilders.delete("/api/car/delete/" + car.getVin())
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service , Mockito.atLeastOnce()).delete(car.getVin());
    }
}