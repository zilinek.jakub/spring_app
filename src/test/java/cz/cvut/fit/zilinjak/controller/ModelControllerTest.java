package cz.cvut.fit.zilinjak.controller;

import cz.cvut.fit.zilinjak.DTO.CategoryCreateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelCreateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelDTO;
import cz.cvut.fit.zilinjak.DTO.ModelDTO;
import cz.cvut.fit.zilinjak.entity.Model;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Model;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.service.ModelService;
import cz.cvut.fit.zilinjak.service.ModelService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@AutoConfigureMockMvc
@SpringBootTest
class ModelControllerTest {

    @MockBean
    private ModelService service;

    @Autowired
    private MockMvc mock;

    @Test
    void allDTO() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name2" , new Timestamp(2008-11-12) , new Type("typeName2") ,
                Collections.singletonList( new Make("Makename2" , "makePlace2")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        ModelDTO modelDTO2 = new ModelDTO(model2.getId() , model2.getName() , model2.getDate() ,
                model2.getType().getId() , model2.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        List<ModelDTO> listDTO = new ArrayList<>();
        listDTO.add(modelDTO); listDTO.add(modelDTO2);


        BDDMockito.given(service.findAllDTO()).willReturn(listDTO);
        mock.perform(MockMvcRequestBuilders.get("/api/model/all")
            .contentType("application/json")
            .accept("application/json"))
            .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(model.getName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(model.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].type_id", CoreMatchers.is(model.getType().getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[0].make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList()))))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(model2.getName())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(model2.getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].type_id", CoreMatchers.is(model2.getType().getId())))
            .andExpect(MockMvcResultMatchers.jsonPath("$[1].make_id", CoreMatchers.is(model2.getMake().stream().map(Make::getId).collect(Collectors.toList()))))
            ;
    }

    @Test
    void findByIdsDTO() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name2" , new Timestamp(2008-11-12) , new Type("typeName2") ,
                Collections.singletonList( new Make("Makename2" , "makePlace2")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        ModelDTO modelDTO2 = new ModelDTO(model2.getId() , model2.getName() , model2.getDate() ,
                model2.getType().getId() , model2.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        List<ModelDTO> listDTO = new ArrayList<>();
        listDTO.add(modelDTO); listDTO.add(modelDTO2);

        BDDMockito.given(service.findByIdsDTO(List.of(0,1))).willReturn(listDTO);
        mock.perform(MockMvcRequestBuilders.get("/api/model/0/1")
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk()).andDo(print())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(model.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(model.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].type_id", CoreMatchers.is(model.getType().getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList()))))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", CoreMatchers.is(model2.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.is(model2.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].type_id", CoreMatchers.is(model2.getType().getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].make_id", CoreMatchers.is(model2.getMake().stream().map(Make::getId).collect(Collectors.toList()))))
        ;

    }

    @Test
    void byIdDTO() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        BDDMockito.given(service.findByIdDTO(model.getId())).willReturn(Optional.of(modelDTO));
        mock.perform(MockMvcRequestBuilders.get("/api/model/" + model.getId())
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(modelDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(modelDTO.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type_id", CoreMatchers.is(modelDTO.getType_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList()))))
        ;
    }

    @Test
    void byTypeName() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        BDDMockito.given(service.findByType(model.getType().getName())).willReturn(List.of(modelDTO));

        mock.perform(MockMvcRequestBuilders.get("/api/model/byType/" + model.getType().getName())
                .contentType("application/json")
                .accept("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", CoreMatchers.is(model.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.is(model.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].type_id", CoreMatchers.is(model.getType().getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList())))
                );
        Mockito.verify(service , Mockito.atLeastOnce()).findByType(model.getType().getName());

    }

    @Test
    void create() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        BDDMockito.given(service.create(Mockito.any(ModelCreateDTO.class))).willReturn(modelDTO);

        String toSend = "{\"name\": \"name\",\"date\": \"2004-20-20\" , \"make\": [2,5] , \"type\": 1}";

        mock.perform(MockMvcRequestBuilders.post("/api/model/create")
                .contentType("application/json")
                .accept("application/json")
                .content(toSend)).andDo(print())
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(modelDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(modelDTO.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type_id", CoreMatchers.is(modelDTO.getType_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList()))));
        Mockito.verify(service , Mockito.atLeastOnce()).create(Mockito.any(ModelCreateDTO.class));
    }

    @Test
    void save() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        BDDMockito.given(service.update(Mockito.anyInt(),Mockito.any(ModelCreateDTO.class))).willReturn(modelDTO);

        String toSend = "{\"name\": \"name2\",\"date\": \"2004-20-20\" , \"make\": [2,5] , \"type\": 1}";

        mock.perform(MockMvcRequestBuilders.put("/api/model/update/"+model.getId())
                .contentType("application/json")
                .accept("application/json")
                .content(toSend))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", CoreMatchers.is(modelDTO.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.is(modelDTO.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.type_id", CoreMatchers.is(modelDTO.getType_id())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.make_id", CoreMatchers.is(model.getMake().stream().map(Make::getId).collect(Collectors.toList()))));
        Mockito.verify(service , Mockito.atLeastOnce()).update(Mockito.anyInt(), Mockito.any(ModelCreateDTO.class));
    }

    @Test
    void delete() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        mock.perform(MockMvcRequestBuilders.delete("/api/model/delete/" + model.getId())
                .contentType("application/json"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        Mockito.verify(service , Mockito.atLeastOnce()).delete(model.getId());
    }
}