package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.MakeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.MakeDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.MakeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MakeServiceTest {

    @Autowired
    private MakeService makeService;

    @MockBean
    private MakeRepository makeRepository;


    @Test
    void findByIdsDTO() {
        Make test1 = new Make("TestName" , "TestPlace");
        Make test2 = new Make("TestName" , "TestPlace");
        Make test3 = new Make("TestName" , "TestPlace");

        MakeDTO testDTO1 = new MakeDTO(test1.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO2 = new MakeDTO(test2.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO3 = new MakeDTO(test3.getId(),"TestName" , "TestPlace");

        List<MakeDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2);list.add(testDTO3);

        List<Integer> ints = new ArrayList<>();
        ints.add(test1.getId()); ints.add(test2.getId()); ints.add(test3.getId());

        BDDMockito.given(makeRepository.findAllById(ints)).willReturn(List.of(test1 , test2 , test3 ));;
        Assertions.assertEquals(list , makeService.findByIdsDTO(ints));

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAllDTO() {
        Make test1 = new Make("TestName" , "TestPlace");
        Make test2 = new Make("TestName" , "TestPlace");
        Make test3 = new Make("TestName" , "TestPlace");

        MakeDTO testDTO1 = new MakeDTO(test1.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO2 = new MakeDTO(test2.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO3 = new MakeDTO(test3.getId(),"TestName" , "TestPlace");

        List<MakeDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2);

        BDDMockito.given(makeRepository.findAll()).willReturn(List.of(test1 , test2 , test3 ));
        // fails because testDTO3 is not added
        Assertions.assertNotEquals(list , makeService.findAllDTO());

        list.add(testDTO3);
        Assertions.assertEquals(list , makeService.findAllDTO());

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIdDTO() {
        Make test1 = new Make("TestName" , "TestPlace");
        Make test2 = new Make("TestName" , "TestPlace");
        Make test3 = new Make("TestName" , "TestPlace");

        MakeDTO testDTO1 = new MakeDTO(test1.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO2 = new MakeDTO(test2.getId(),"TestName" , "TestPlace");
        MakeDTO testDTO3 = new MakeDTO(test3.getId(),"TestName" , "TestPlace");

        List<MakeDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2);

        List<Integer> ints = new ArrayList<>();
        ints.add(test1.getId()); ints.add(test2.getId());

        BDDMockito.given(makeRepository.findAllById(ints)).willReturn(List.of(test1 , test2 ));
        Assertions.assertEquals(list , makeService.findByIdsDTO(ints));

        ints.add(test3.getId());
        list.add(testDTO3);
        BDDMockito.given(makeRepository.findAllById(ints)).willReturn(List.of(test1 , test2 ,test3));
        Assertions.assertEquals(list , makeService.findByIdsDTO(ints));

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findByIds() {
        Make test1 = new Make("TestName" , "TestPlace");
        Make test2 = new Make("TestName" , "TestPlace");
        Make test3 = new Make("TestName" , "TestPlace");

        List<Make> list = new ArrayList<>();
        list.add(test1); list.add(test2);

        List<Integer> ints = new ArrayList<>();
        ints.add(test1.getId()); ints.add(test2.getId());

        BDDMockito.given(makeRepository.findAllById(ints)).willReturn(List.of(test1 , test2 ));
        Assertions.assertEquals(list , makeService.findByIds(ints));

        ints.add(test3.getId());
        list.add(test3);
        BDDMockito.given(makeRepository.findAllById(ints)).willReturn(List.of(test1 , test2 ,test3));
        Assertions.assertEquals(list , makeService.findByIds(ints));

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAll() {
        Make test1 = new Make("TestName" , "TestPlace");
        Make test2 = new Make("TestName" , "TestPlace");
        Make test3 = new Make("TestName" , "TestPlace");
        List<Make> list = new ArrayList<>();
        list.add(test1); list.add(test2); list.add(test3);
        BDDMockito.given(makeRepository.findAll()).willReturn(List.of(test1 , test2 , test3));
        Assertions.assertEquals(list , makeService.findAll());
        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findById() {
        Make make = new Make("Make" , "TestPlace");
        BDDMockito.given(makeRepository.findById(make.getId())).willReturn(Optional.of(make));
        Assertions.assertEquals(make, makeService.findById(make.getId()).get());
        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findById(make.getId());
    }

    @Test
    void create() {
        Make make = new Make("Make" , "TestPlace");
        MakeCreateDTO create = new MakeCreateDTO(make.getName() , make.getPlace());
        MakeDTO dto = new MakeDTO(make.getId() , make.getName() , make.getPlace());

        BDDMockito.given(makeRepository.save(make)).willReturn(make);

        Assertions.assertEquals( makeService.create(create).getId() , dto.getId() );

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).save(make);
    }

    @Test
    void update() throws Exception {

        Make make = new Make("Make" , "TestPlace");
        MakeCreateDTO makeCreate = new MakeCreateDTO("Make2" , "TestPlace2");

        BDDMockito.given(makeRepository.findById(make.getId())).willReturn(Optional.of(make));

        Assertions.assertNotEquals(make.getName() , makeCreate.getName() );
        makeService.update(make.getId(), makeCreate);
        Assertions.assertEquals(make.getName() , makeCreate.getName() );

        Mockito.verify(makeRepository, Mockito.atLeastOnce()).findById(make.getId());
    }

    @Test
    void delete() throws Exception {
        Make make = new Make("Make" , "TestPlace");
        BDDMockito.given(makeRepository.findById(make.getId())).willReturn(Optional.of(make));
        makeService.delete(make.getId());
        Mockito.verify(makeRepository, Mockito.atLeastOnce()).delete(make);
    }
}