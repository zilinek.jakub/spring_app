package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.CarCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CarDTO;
import cz.cvut.fit.zilinjak.DTO.CarUpdateDTO;
import cz.cvut.fit.zilinjak.entity.*;
import cz.cvut.fit.zilinjak.repository.CarRepository;
import cz.cvut.fit.zilinjak.repository.CategoryRepository;
import cz.cvut.fit.zilinjak.repository.ModelRepository;
import javassist.expr.NewArray;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CarServiceTest {
    @Autowired
    CarService service;

    @MockBean
    CarRepository carRepository;

    @MockBean
    ModelRepository modelRepository;
    @MockBean
    CategoryRepository categoryRepository;

    Make make;
    Type type;
    Category cat;
    Model model;

    @BeforeEach
    void init(){
        make = new Make("Makename" , "makePlace");
        type = new Type("typeName");
        cat = new Category("kategorie");
        model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList(make));
    }
    @Test
    void findByIdDTO() {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        CarDTO carDTO = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,
                car.getFuel_type() , car.getModel().getId() ,car.getCategory().getId() , car.getCategory().getId() );
        BDDMockito.given(carRepository.findById(car.getVin())).willReturn(Optional.of(car));
        Assertions.assertEquals(service.findByIdDTO(car.getVin()).get(), carDTO);
    }
    @Test
    void findAllDTO() {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        Car car2 = new Car("vin2" , "color" , 333 , true ,
                "petrolHead" , model ,0 , cat );
        Car car3 = new Car("vin1" , "color" , 666 , true ,
                "petrolHead" , model ,0 , cat );
        CarDTO carDTO = new CarDTO(car.getVin() , car.getColor() , car.getPrice() , car.isStk() ,
                car.getFuel_type() , car.getModel().getId() ,car.getCategory().getId() , car.getCategory().getId() );
        CarDTO carDTO2 = new CarDTO(car2.getVin() , car2.getColor() , car2.getPrice() , car2.isStk() ,
                car2.getFuel_type() , car2.getModel().getId() ,car2.getCategory().getId() , car2.getCategory().getId() );
        CarDTO carDTO3 = new CarDTO(car3.getVin() , car3.getColor() , car3.getPrice() , car3.isStk() ,
                car3.getFuel_type() , car3.getModel().getId() ,car3.getCategory().getId() , car3.getCategory().getId() );

        List<Car> list = new ArrayList<>(
                List.of(car , car2 , car3)
        );
        List<CarDTO> listdto = new ArrayList<>(
                List.of(carDTO , carDTO2 , carDTO3)
        );

        BDDMockito.given(carRepository.findAll()).willReturn(list);
        Assertions.assertEquals(service.findAllDTO() , listdto);
        Mockito.verify(carRepository, Mockito.atLeastOnce()).findAll();
    }
    @Test
    void findById() {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        BDDMockito.given(carRepository.findById(car.getVin())).willReturn(Optional.of(car));

        Assertions.assertEquals(service.findById(car.getVin()).get() , car);

        Mockito.verify(carRepository, Mockito.atLeastOnce()).findById(car.getVin());
    }
    @Test
    void findAll() {
        Car car = new Car("vin666" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        Car car2 = new Car("vin2" , "color" , 333 , true ,
                "petrolHead" , model ,0 , cat );
        Car car3 = new Car("vin1" , "color" , 666 , true ,
                "petrolHead" , model ,0 , cat );

        List<Car> list = new ArrayList<>(
                List.of(car , car2 , car3)
        );

        BDDMockito.given(carRepository.findAll()).willReturn(list);

        Assertions.assertEquals(service.findAll() , list);
        Mockito.verify(carRepository, Mockito.atLeastOnce()).findAll();
    }
    @Test
    void create() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        CarCreateDTO create = new CarCreateDTO(car.getVin() , car.getColor() ,car.getPrice(),
        car.isStk(), car.getCategory().getId() , car.getFuel_type() ,car.getModel().getId() , car.getDriven());
        CarDTO dto = new CarDTO(create.getVin() , create.getColor() , create.getPrice() , create.isStk() ,create.getFuel_type(),
                create.getModel_id() , create.getCategory_id() , create.getDriven());

        BDDMockito.given(carRepository.save(car)).willReturn(car);
        BDDMockito.given(modelRepository.findById(create.getModel_id())).willReturn(Optional.of(model));
        BDDMockito.given(categoryRepository.findById(create.getCategory_id())).willReturn(Optional.of(cat));

        CarDTO test = service.create(create);
        Assertions.assertEquals( test , dto );

        /* How else should i test this? */
        assertEquals( test.getVin(), dto.getVin() );
        assertEquals( test.getFuel_type(), dto.getFuel_type() );
        assertEquals( test.getColor(), dto.getColor() );
        assertEquals( test.getCategory_id(), dto.getCategory_id() );
        assertEquals( test.getDriven(), dto.getDriven() );
        assertEquals( test.getModel_id(), dto.getModel_id() );
        assertEquals( test.getPrice(), dto.getPrice() );
        /* How else should i test this? */

        Mockito.verify(carRepository, Mockito.atLeastOnce()).save(car);
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findById(create.getModel_id());
        Mockito.verify(categoryRepository, Mockito.atLeastOnce()).findById(create.getCategory_id());
    }
    @Test
    void update() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true ,
                "petrolHead" , model ,0 , cat );
        CarUpdateDTO update = new CarUpdateDTO("New Color", car.getPrice() , car.isStk() ,
                car.getFuel_type() , car.getModel().getId() ,car.getCategory().getId() , car.getDriven());
        CarDTO dto = new CarDTO(car.getVin() , /*IMPORTANT*/ update.getColor() , car.getPrice() , car.isStk() ,car.getFuel_type(),
                car.getModel().getId() , car.getCategory().getId() , car.getDriven());


        BDDMockito.given(carRepository.findById(car.getVin())).willReturn(Optional.of(car));
        BDDMockito.given(modelRepository.findById(update.getModel_id())).willReturn(Optional.of(model));
        BDDMockito.given(categoryRepository.findById(update.getCategory_id())).willReturn(Optional.of(cat));

        assertNotEquals( car.getColor(), dto.getColor() );
        assertEquals( car.getVin(), dto.getVin() );
        service.update(car.getVin(), update);

        assertEquals( car.getColor(), dto.getColor() );
        assertEquals( car.getVin(), dto.getVin() );

        Mockito.verify(carRepository, Mockito.atLeastOnce()).findById(car.getVin());
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findById(update.getModel_id());
        Mockito.verify(categoryRepository, Mockito.atLeastOnce()).findById(update.getCategory_id());
    }
    @Test
    void delete() throws Exception {
        Car car = new Car("vin" , "color" , 199 , true , "petrolHead" , model ,
                0 , cat );

        BDDMockito.given(carRepository.findById(car.getVin())).willReturn(Optional.of(car));
        service.delete(car.getVin());
        Mockito.verify(carRepository, Mockito.atLeastOnce()).delete(car);
    }
    @Test
    void Test(){
        assertEquals(1,1);
    }
}