package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.ModelCreateDTO;
import cz.cvut.fit.zilinjak.DTO.ModelDTO;
import cz.cvut.fit.zilinjak.entity.Make;
import cz.cvut.fit.zilinjak.entity.Model;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.ModelRepository;
import cz.cvut.fit.zilinjak.repository.TypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ModelServiceTest {

    @Autowired
    ModelService service;

    @MockBean
    private TypeRepository typeRepository;
    @MockBean
    private ModelRepository modelRepository;
    @MockBean
    private MakeService makeService;

    @Test
    void findByIdsDTO() {

        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name2" , new Timestamp(2008-11-12) , new Type("typeName2") ,
                Collections.singletonList( new Make("Makename2" , "makePlace2")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        ModelDTO modelDTO2 = new ModelDTO(model2.getId() , model2.getName() , model2.getDate() ,
                model2.getType().getId() , model2.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        List<ModelDTO> listDTO = new ArrayList<>();
        listDTO.add(modelDTO); listDTO.add(modelDTO2);

        List<Model> list = new ArrayList<>();
        list.add(model); list.add(model2);

        List<Integer> ints = new ArrayList<>();
        ints.add(model.getId()); ints.add(model2.getId());

        BDDMockito.given(modelRepository.findAllById(ints)).willReturn(list);
        Assertions.assertEquals(service.findByIdsDTO(ints) , listDTO);
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findAllById(ints);

    }

    @Test
    void findAllDTO() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name2" , new Timestamp(2008-11-12) , new Type("typeName2") ,
                Collections.singletonList( new Make("Makename2" , "makePlace2")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        ModelDTO modelDTO2 = new ModelDTO(model2.getId() , model2.getName() , model2.getDate() ,
                model2.getType().getId() , model2.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        List<ModelDTO> listDTO = new ArrayList<>();
        listDTO.add(modelDTO); listDTO.add(modelDTO2);

        List<Model> list = new ArrayList<>();
        list.add(model); list.add(model2);

        BDDMockito.given(modelRepository.findAll()).willReturn(list);
        Assertions.assertEquals(service.findAllDTO() , listDTO);
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findAll();

    }

    @Test
    void findByIdDTO() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        BDDMockito.given(modelRepository.findById(model.getId())).willReturn(Optional.of(model));
        Assertions.assertEquals( service.findByIdDTO(model.getId()).get() , modelDTO);
        Mockito.verify(modelRepository , Mockito.atLeastOnce()).findById(model.getId());
    }

    @Test
    void findByIds() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));

        List<Model> list = new ArrayList<>();
        list.add(model); list.add(model2);

        List<Integer> ints = new ArrayList<>();
        ints.add(model.getId()); ints.add(model2.getId());

        BDDMockito.given(modelRepository.findAllById(ints)).willReturn(list);

        Assertions.assertEquals(service.findByIds(ints) , list);

        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAll() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));

        List<Model> list = new ArrayList<>();
        list.add(model); list.add(model2);
        BDDMockito.given(modelRepository.findAll()).willReturn(list);
        Assertions.assertEquals(service.findAll() , list);
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findById() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        BDDMockito.given(modelRepository.findById(model.getId())).willReturn(Optional.of(model));
        Assertions.assertEquals( service.findById(model.getId()).get() , model);
        Mockito.verify(modelRepository , Mockito.atLeastOnce()).findById(model.getId());
    }

    @Test
    void create() throws Exception {

        Make make = new Make("Makename" , "makePlace");
        Type type = new Type("typeName");

        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList(make));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));

        ModelCreateDTO update = new ModelCreateDTO(model.getName() , model.getDate() , type.getId() ,
                Collections.singletonList( make.getId()));

        /*===========*/
        BDDMockito.given(modelRepository.save(model)).willReturn(model);
        BDDMockito.given(typeRepository.findById(update.getType_id())).willReturn(Optional.of(type));
        BDDMockito.given(makeService.findByIds(update.getMake_id())).willReturn(List.of(make));
        /*===========*/

        Assertions.assertEquals( service.create(update) , modelDTO );

        Mockito.verify(modelRepository, Mockito.atLeastOnce()).save(model);
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findById(update.getType_id());
        Mockito.verify(makeService, Mockito.atLeastOnce()).findByIds(update.getMake_id());

    }

    @Test
    void update() throws Exception {
        Make make = new Make("Makename" , "makePlace");
        Type type = new Type("typeName");

        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList(make));

        ModelCreateDTO update = new ModelCreateDTO("name2" , new Timestamp(2008-11-12) , type.getId() ,
                Collections.singletonList( make.getId()));
        /*===========*/
        BDDMockito.given(modelRepository.findById(model.getId())).willReturn(Optional.of(model));
        BDDMockito.given(typeRepository.findById(update.getType_id())).willReturn(Optional.of(type));
        BDDMockito.given(makeService.findByIds(update.getMake_id())).willReturn(List.of(make));
        /*===========*/

        Assertions.assertNotEquals(model.getDate() , update.getDate() );
        Assertions.assertEquals(model.getId() , service.update(model.getId() , update).getId() ); // tests if Id is same
        Assertions.assertEquals(model.getDate() , service.update(model.getId() , update).getDate() ); // tests if date has changed
        Assertions.assertEquals(model.getName() , service.update(model.getId() , update).getName() ); // tests if name has changed

        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findById(model.getId());
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findById(update.getType_id());
        Mockito.verify(makeService, Mockito.atLeastOnce()).findByIds(update.getMake_id());
    }

    @Test
    void delete() throws Exception {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        BDDMockito.given(modelRepository.findById(model.getId())).willReturn(Optional.of(model));
        service.delete(model.getId());
        Mockito.verify(modelRepository, Mockito.atLeastOnce()).delete(model);
    }

    @Test
    void findByType() {
        Model model = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));
        Model model2 = new Model("name" , new Timestamp(2008-11-11) , new Type("typeName") ,
                Collections.singletonList( new Make("Makename" , "makePlace")));

        ModelDTO modelDTO = new ModelDTO(model.getId() , model.getName() , model.getDate() ,
                model.getType().getId() , model.getMake().stream().map(Make::getId).collect(Collectors.toList()));
        ModelDTO modelDTO2 = new ModelDTO(model2.getId() , model2.getName() , model2.getDate() ,
                model2.getType().getId() , model2.getMake().stream().map(Make::getId).collect(Collectors.toList()));



        List<Model> listNoDTO = new ArrayList<>();
        listNoDTO.add(model); listNoDTO.add(model2);

        List<ModelDTO> list = new ArrayList<>();
        list.add(modelDTO); list.add(modelDTO2);

        BDDMockito.given(modelRepository.findByType("typeName")).willReturn(listNoDTO);
        Assertions.assertEquals(service.findByType("typeName") , list);

        Mockito.verify(modelRepository, Mockito.atLeastOnce()).findByType("typeName");


    }
}