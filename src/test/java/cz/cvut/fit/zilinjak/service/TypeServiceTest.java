package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.TypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
class TypeServiceTest {

    @Autowired
    private TypeService typeService;

    @MockBean
    TypeRepository typeRepository;

    @Test
    void findByIdsDTO() {

        Type testType1 = new Type("TestName");
        Type testType2 = new Type("TestName");
        Type testType3 = new Type("TestName");

        TypeDTO testDTO2 = new TypeDTO(testType1.getId(), "TestName");
        TypeDTO testDTO3 = new TypeDTO(testType2.getId(), "TestName");
        TypeDTO testDTO1 = new TypeDTO(testType3.getId(), "TestName");

        List<TypeDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2); list.add(testDTO3);

        List<Integer> ints = new ArrayList<>();
        ints.add(testType1.getId()); ints.add(testType2.getId()); ints.add(testType3.getId());

        BDDMockito.given(typeRepository.findAllById(ints)).willReturn(List.of(testType1 , testType2 , testType3));
        Assertions.assertEquals(list , typeService.findByIdsDTO(ints));
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findAllById(ints);

    }

    @Test
    void findAllDTO() {

        Type testType1 = new Type("TestName");
        Type testType2 = new Type("TestName");
        Type testType3 = new Type("TestName");

        TypeDTO testDTO2 = new TypeDTO(testType1.getId(), "TestName");
        TypeDTO testDTO3 = new TypeDTO(testType2.getId(), "TestName");
        TypeDTO testDTO1 = new TypeDTO(testType3.getId(), "TestName");

        List<TypeDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2); list.add(testDTO3);

        BDDMockito.given(typeRepository.findAll()).willReturn(List.of(testType1 , testType2 , testType3));
        Assertions.assertEquals(list , typeService.findAllDTO());
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIdDTO() {
        Type testType = new Type("TestName");
        TypeDTO testTypeDTO = new TypeDTO( testType.getId(),"TestName");
        BDDMockito.given(typeRepository.findById(testType.getId())).willReturn(Optional.of(testType));
        Assertions.assertEquals( testTypeDTO, typeService.findByIdDTO(testType.getId()).get());
    }

    @Test
    void findByIds() {
        Type testType1 = new Type("TestName1");
        Type testType2 = new Type("TestName2");
        Type testType3 = new Type("TestName3");

        List<Type> list = new ArrayList<>();
        list.add(testType1); list.add(testType2); list.add(testType3);

        List<Integer> ints = new ArrayList<>();
        ints.add(testType1.getId()); ints.add(testType2.getId()); ints.add(testType3.getId());

        BDDMockito.given(typeRepository.findAllById(ints)).willReturn(List.of(testType1 , testType2 , testType3));
        Assertions.assertEquals(list , typeService.findByIds(ints));
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAll() {
        Type testType1 = new Type("TestName");
        Type testType2 = new Type("TestName");
        Type testType3 = new Type("TestName");
        List<Type> list = new ArrayList<>();
        list.add(testType1); list.add(testType2); list.add(testType3);
        BDDMockito.given(typeRepository.findAll()).willReturn(List.of(testType1 , testType2 , testType3));
        Assertions.assertEquals(list , typeService.findAll());
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findById() {
        Type testType = new Type("TestName");
        BDDMockito.given(typeRepository.findById(testType.getId())).willReturn(Optional.of(testType));
        Assertions.assertEquals(testType, typeService.findById(testType.getId()).get());
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findById(testType.getId());
    }
    @Test
    void create() {
        Type testType = new Type("TestName");

        TypeCreateDTO createDTO = new TypeCreateDTO( testType.getName() );
        TypeDTO testTypeDTO = new TypeDTO( testType.getId() , testType.getName() );

        BDDMockito.given(typeRepository.save(testType)).willReturn(testType);

        Assertions.assertEquals( typeService.create(createDTO) , testTypeDTO );
        Assertions.assertEquals( typeService.create(createDTO).getName() , testTypeDTO.getName() );
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).save(testType);
    }
    @Test
    void update() throws Exception {
        Type testType = new Type("TestName");
        TypeCreateDTO createDTO = new TypeCreateDTO("TestNameNew");

        BDDMockito.given(typeRepository.findById(testType.getId())).willReturn(Optional.of(testType));
        Assertions.assertNotEquals(testType.getName() , createDTO.getName() );
        typeService.update(testType.getId(), createDTO);
        Assertions.assertEquals(testType.getName() , createDTO.getName() );

        Mockito.verify(typeRepository, Mockito.atLeastOnce()).findById(testType.getId());
    }
    @Test
    void delete() throws Exception {
        Type testType = new Type("TestName");
        BDDMockito.given(typeRepository.findById(testType.getId())).willReturn(Optional.of(testType));
        typeService.delete(testType.getId());
        Mockito.verify(typeRepository, Mockito.atLeastOnce()).delete(testType);
    }
}