package cz.cvut.fit.zilinjak.service;

import cz.cvut.fit.zilinjak.DTO.CategoryCreateDTO;
import cz.cvut.fit.zilinjak.DTO.CategoryDTO;
import cz.cvut.fit.zilinjak.DTO.TypeCreateDTO;
import cz.cvut.fit.zilinjak.DTO.TypeDTO;
import cz.cvut.fit.zilinjak.entity.Category;
import cz.cvut.fit.zilinjak.entity.Type;
import cz.cvut.fit.zilinjak.repository.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CategoryServiceTest {

    @Autowired
    CategoryService service;

    @MockBean
    CategoryRepository repository;

    @Test
    void findByIdsDTO() {
        Category test = new Category("TestName");
        Category test2 = new Category("TestName");
        Category test3 = new Category("TestName");

        CategoryDTO testDTO3 = new CategoryDTO(test2.getId(), "TestName");
        CategoryDTO testDTO1 = new CategoryDTO(test3.getId(), "TestName");
        CategoryDTO testDTO2 = new CategoryDTO(test.getId(), "TestName");

        List<CategoryDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2); list.add(testDTO3);

        List<Integer> ints = new ArrayList<>();
        ints.add(test.getId()); ints.add(test2.getId()); ints.add(test3.getId());

        BDDMockito.given(repository.findAllById(ints)).willReturn(List.of(test , test2 , test3));
        Assertions.assertEquals(list , service.findByIdsDTO(ints));
        Mockito.verify(repository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAllDTO() {
        Category test = new Category("TestName");
        Category test2 = new Category("TestName");
        Category test3 = new Category("TestName");

        CategoryDTO testDTO2 = new CategoryDTO(test.getId(), "TestName");
        CategoryDTO testDTO3 = new CategoryDTO(test2.getId(), "TestName2");
        CategoryDTO testDTO1 = new CategoryDTO(test3.getId(), "TestName3");

        List<CategoryDTO> list = new ArrayList<>();
        list.add(testDTO1); list.add(testDTO2); list.add(testDTO3);

        BDDMockito.given(repository.findAll()).willReturn(List.of(test , test2 , test3));
        Assertions.assertEquals(list , service.findAllDTO());
        Mockito.verify(repository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIdDTO() {
        Category test = new Category("TestName");
        CategoryDTO testDTO = new CategoryDTO( test.getId(),"TestName");
        BDDMockito.given(repository.findById(test.getId())).willReturn(Optional.of(test));
        Assertions.assertEquals( testDTO, service.findByIdDTO(test.getId()).get());
    }

    @Test
    void findByIds() {
        Category test1 = new Category("TestName1");
        Category test2 = new Category("TestName2");
        Category test3 = new Category("TestName3");

        List<Category> list = new ArrayList<>();
        list.add(test1); list.add(test2); list.add(test3);

        List<Integer> ints = new ArrayList<>();
        ints.add(test1.getId()); ints.add(test2.getId()); ints.add(test3.getId());

        BDDMockito.given(repository.findAllById(ints)).willReturn(List.of(test1 , test2 , test3));
        Assertions.assertEquals(list , service.findByIds(ints));
        Mockito.verify(repository, Mockito.atLeastOnce()).findAllById(ints);
    }

    @Test
    void findAll() {
        Category testType2 = new Category("TestName");
        Category testType3 = new Category("TestName");
        Category testType1 = new Category("TestName");
        List<Category> list = new ArrayList<>();
        list.add(testType1); list.add(testType2); list.add(testType3);

        BDDMockito.given(repository.findAll()).willReturn(List.of(testType1 , testType2 , testType3));
        Assertions.assertEquals(list , service.findAll());
        Mockito.verify(repository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findById() {
        Category testType = new Category("TestName");
        BDDMockito.given(repository.findById(testType.getId())).willReturn(Optional.of(testType));
        Assertions.assertEquals(testType, service.findById(testType.getId()).get());
        Mockito.verify(repository, Mockito.atLeastOnce()).findById(testType.getId());
    }

    @Test
    void create() {
        Category test = new Category("TestName");
        CategoryDTO testDTO = new CategoryDTO( test.getId() , test.getName() );
        CategoryCreateDTO createDTO = new CategoryCreateDTO( test.getName() );

        BDDMockito.given(repository.save(test)).willReturn(test);

        Assertions.assertEquals( service.create(createDTO), testDTO );

        Mockito.verify(repository, Mockito.atLeastOnce()).save(test);
    }

    @Test
    void update() throws Exception {
        Category cat = new Category("TestName");
        CategoryCreateDTO createDTO = new CategoryCreateDTO("TestNameNew");

        BDDMockito.given(repository.findById(cat.getId())).willReturn(Optional.of(cat));
        Assertions.assertNotEquals(cat.getName() , createDTO.getName() );
        service.update(cat.getId(), createDTO);
        Assertions.assertEquals(cat.getName() , createDTO.getName() );

        Mockito.verify(repository, Mockito.atLeastOnce()).findById(cat.getId());
    }

    @Test
    void delete() throws Exception {
        Category cat = new Category("TEST");
        BDDMockito.given(repository.findById(cat.getId())).willReturn(Optional.of(cat));
        service.delete(cat.getId());
        Mockito.verify(repository, Mockito.atLeastOnce()).delete(cat);
        Mockito.verify(repository, Mockito.atLeastOnce()).findById(cat.getId());
    }
}